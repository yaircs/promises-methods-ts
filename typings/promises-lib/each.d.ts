export declare function each(promiseIterable: Iterable<unknown> | Promise<Iterable<unknown>>, callback: (item: unknown, index: number, length: number) => unknown): Promise<unknown[]>;
