export declare function mapParallel(promiseIterable: Iterable<any> | Promise<Iterable<any>>, callback: (item: unknown) => unknown): Promise<Array<unknown>>;
