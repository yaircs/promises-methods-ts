export interface pObj {
    [key: string]: Promise<any> | unknown;
}
export declare function props(promisesObj: pObj): Promise<pObj>;
