/// <reference types="mocha" />
/// <reference types="node" />
export declare const delay: (ms: number) => Promise<NodeJS.Timeout>;
export declare const echo: (msg: string, ms: number) => Promise<string>;
export declare const random: (max: number, min?: number) => number;
