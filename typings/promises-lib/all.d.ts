export declare function all(resolvedArr: Iterable<any> | Promise<Iterable<any>>): Promise<Array<unknown>>;
