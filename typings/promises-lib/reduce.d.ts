export declare function reduce(promiseIterable: Iterable<any> | Promise<Iterable<any>>, callback: (agg: unknown, item: unknown) => unknown, initial: unknown): Promise<Array<unknown>>;
