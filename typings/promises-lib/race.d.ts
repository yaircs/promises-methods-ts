export declare function race(promiseIterable: Iterable<any> | Promise<Iterable<any>>): Promise<any>;
