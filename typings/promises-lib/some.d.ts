export declare function some(promiseIterable: Iterable<any> | Promise<Iterable<any>>, count: number): Promise<any>;
