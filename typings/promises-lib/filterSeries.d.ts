export declare function filterSeries(promiseIterable: Iterable<any> | Promise<Iterable<any>>, callback: (item: unknown, index: number) => unknown): Promise<Array<unknown>>;
