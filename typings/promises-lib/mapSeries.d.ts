export declare function mapSeries(promiseIterable: Iterable<unknown> | Promise<Iterable<unknown>>, callback: (item: unknown, index: number) => unknown): Promise<Array<unknown>>;
