import { expect } from "chai";
import { some } from "../src/promises-lib/some";
import { delay } from '../src/promises-lib/utils';

function createPromise(operation: string, ms: number) {
    return new Promise((resolve, reject) => {
        delay(ms)
            .then(() => {
                if (operation === "reject") {
                    throw new Error("Promise rejected");
                }
                resolve(operation);
            })
            .catch((err)=>reject(err.message));
    });
}

describe("tests of 'some' function", () => {
    context("some module", () => {

        it("should be a function", () => {
            expect(some).to.be.a("function");
            expect(some).to.be.a.instanceOf(Function);
        }),

        it("accepts an array of promises should return a resolved arr in the size of count parameter", async () => {
            const pArr = [];
            for (let i=0; i<5; i++) {
                pArr.push(new Promise((resolve, reject) => {
                    return setTimeout(() => {
                        return resolve(i);
                    }, 1000-i);
                }));
            };
            
            expect(await some(pArr, 2)).to.deep.equal([4,3]);
        }),

        it("accepts a mixed array of promises and primitives and should return a resolved arr in the size of count parameter", async () => {
            const pArr = [];
            for (let i=0; i<5; i++) {
                pArr.push(new Promise((resolve, reject) => {
                    return setTimeout(() => {
                        return resolve(i);
                    }, 1000-i);
                }));
            };
            pArr.push(5);

            expect(await some(pArr, 2)).to.deep.equal([5,4]);
        }),

        it("should throw an error",  async function () {
            try{
                await some([2, createPromise("reject", 1000), 1], 2);
            }catch(err){
                expect(err).to.be.equal("Promise rejected");
            }
        });
    });
});