import { expect } from "chai";
import { each } from "../src/promises-lib/each";
import { delay } from '../src/promises-lib/utils';

function createPromise(operation: string, ms: number) {
    return new Promise((resolve, reject) => {
        delay(ms)
            .then(() => {
                if (operation === "reject") {
                    throw new Error("Promise rejected");
                }
                resolve(operation);
            })
            .catch((err)=>reject(err.message));
    });
}

describe("tests of 'each' function", () => {
    context("each module", () => {

        it("should be a function", () => {
            expect(each).to.be.a("function");
            expect(each).to.be.a.instanceOf(Function);
        }),

        it("accepts and array of promises and should return a resolved arr", async () => {
            const pArr = [];
            for (let i=0; i<5; i++) {
                pArr.push(new Promise((resolve, reject) => {
                    return setTimeout(() => {
                        return resolve(i);
                    }, 0);
                }));
            };
            
            expect(await each(pArr, async (item) => {
                return await item;
            })).to.deep.equal([0,1,2,3,4]);
        });

        it("accepts a mixed array of promises and primitives and should return a resolved arr", async () => {
            const pArr = [];
            for (let i=0; i<5; i++) {
                pArr.push(new Promise((resolve, reject) => {
                    return setTimeout(() => {
                        return resolve(i);
                    }, 0);
                }));
            }
            pArr.push("q");
            pArr.push("p");
            
            expect(await each(pArr, async (item) => {
                return await item;
            })).to.deep.equal([0,1,2,3,4,"q","p"]);
        }),

        it("should throw an error",  async function () {
            try{
                await each([2, createPromise("reject", 1000), 1], async (item) => {
                    return await item;
                });
            }catch(err){
                expect(err).to.be.equal("Promise rejected");
            }
        });
    });
});