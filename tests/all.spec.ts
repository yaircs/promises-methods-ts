import { doesNotMatch } from "assert/strict";
import { expect } from "chai";
import { all } from "../src/promises-lib/all";
import { delay } from "../src/promises-lib/utils";


function createPromise(operation: string, ms: number) {
    return new Promise((resolve, reject) => {
        delay(ms)
            .then(() => {
                if (operation === "reject") {
                    throw new Error("Promise rejected");
                }
                resolve(operation);
            })
            .catch((err)=>reject(err.message));
    });
}

describe("tests of 'all' function", () => {
    context("all module", () => {

        it("should be a function", () => {
            expect(all).to.be.a("function");
            expect(all).to.be.a.instanceOf(Function);
        }),

        it("accepts an array of promises should return a resolved arr", async () => {
            const pArr = [];
            for (let i=0; i<5; i++) {
                pArr.push(new Promise((resolve, reject) => {
                    return setTimeout(() => {
                        return resolve(1);
                    }, 0);
                }));
            };
            
            expect(await all(pArr)).to.deep.equal([1,1,1,1,1]);
        });

        it("accepts a mixed array of promises and primitives and should return a resolved arr", async () => {
            const pArr = [];
            for (let i=0; i<5; i++) {
                pArr.push(new Promise((resolve, reject) => {
                    return setTimeout(() => {
                        return resolve(i);
                    }, 0);
                }));
            };
            pArr.push("p");
            pArr.push("q");            ;
            expect(await all(pArr)).to.deep.equal([0,1,2,3,4,"p","q"]);
        }),

        it("should throw an error",  async function () {
            try{
                await all([2, createPromise("reject", 1000), 1]);
            }catch(err){
                expect(err).to.be.equal("Promise rejected");
            }
        });
    });
});

