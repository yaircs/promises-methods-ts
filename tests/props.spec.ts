import { expect } from "chai";
import { props } from "../src/promises-lib/props";
import { pObj } from "../src/promises-lib/props";
import { delay } from '../src/promises-lib/utils';

function createPromise(operation: string, ms: number) {
    return new Promise((resolve, reject) => {
        delay(ms)
            .then(() => {
                if (operation === "reject") {
                    throw new Error("Promise rejected");
                }
                resolve(operation);
            })
            .catch((err)=>reject(err.message));
    });
}

describe("tests of 'props' function", () => {
    context("props module", () => {

        it("should be a function", () => {
            expect(props).to.be.a("function");
            expect(props).to.be.a.instanceOf(Function);
        }),

        it("accepts an object of promises should return a resolved obj", async () => {
            const pArr: pObj = {};
            for (let i=0; i<5; i++) {
                pArr[i] = new Promise((resolve, reject) => {
                    return setTimeout(() => {
                        return resolve(1);
                    }, 0);
                });
            };
            
            expect(await props(pArr)).to.deep.equal({
                0: 1,
                1: 1,
                2: 1,
                3: 1,
                4: 1
            });
        }),

        it("accepts a mixed object of promises and primitives and should return a resolved arr", async () => {
            const pArr: pObj = {};
            for (let i=0; i<5; i++) {
                pArr[i] = new Promise((resolve, reject) => {
                    return setTimeout(() => {
                        return resolve(i);
                    }, 0);
                });
            };
            pArr[5] = 10;
            pArr[6] = 11;

            expect(await props(pArr)).to.deep.equal({
                0: 0,
                1: 1,
                2: 2,
                3: 3,
                4: 4,
                5: 10,
                6: 11
            });
        }),

        it("should throw an error",  async function () {
            try{
                await props({0: 2, 1:createPromise("reject", 1000), 2:1});
            }catch(err){
                expect(err).to.be.equal("Promise rejected");
            }
        });
    });
});
