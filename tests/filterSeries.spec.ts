import { expect } from "chai";
import { filterSeries } from "../src/promises-lib/filterSeries";
import { delay } from '../src/promises-lib/utils';

function createPromise(operation: string, ms: number) {
    return new Promise((resolve, reject) => {
        delay(ms)
            .then(() => {
                if (operation === "reject") {
                    throw new Error("Promise rejected");
                }
                resolve(operation);
            })
            .catch((err)=>reject(err.message));
    });
}

describe("tests of 'filterSeries' function", () => {
    context("filterSeries module", () => {

        it("should be a function", () => {
            expect(filterSeries).to.be.a("function");
            expect(filterSeries).to.be.a.instanceOf(Function);
        }),

        it("accepts an array of promises and should return a filtered arr", async () => {
            const pArr = [];
            for (let i=0; i<5; i++) {
                pArr.push(new Promise((resolve, reject) => {
                    return setTimeout(() => {
                        return resolve(i);
                    }, 0);
                }));
            };

            expect(await filterSeries(pArr, async (item) => {
                return (await item as number) > 2;
            })).to.deep.equal([3,4]);
        }),

        it("accepts a mixed array of promises and prmitives and should return a filtered arr", async () => {
            const pArr = [];
            for (let i=0; i<5; i++) {
                pArr.push(new Promise((resolve, reject) => {
                    return setTimeout(() => {
                        return resolve(i);
                    }, 0);
                }));
            };
            pArr.push(6);
            pArr.push(7);
            
            expect(await filterSeries(pArr, async (item) => {
                return (await item as number) > 2;
            })).to.deep.equal([3,4,6,7]);
        }),

        it("should throw an error",  async function () {
            try{
                await filterSeries([2, createPromise("reject", 1000), 1], async (item) => {
                    return (await item as Number) > 1;
                });
            }catch(err){
                expect(err).to.be.equal("Promise rejected");
            }
        });
    });
});