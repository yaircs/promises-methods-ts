import { expect } from "chai";
import { reduce } from "../src/promises-lib/reduce";
import { delay } from '../src/promises-lib/utils';

function createPromise(operation: string, ms: number) {
    return new Promise((resolve, reject) => {
        delay(ms)
            .then(() => {
                if (operation === "reject") {
                    throw new Error("Promise rejected");
                }
                resolve(operation);
            })
            .catch((err)=>reject(err.message));
    });
}

describe("tests of 'reduce' function", () => {
    context("reduce module", () => {

        it("should be a function", () => {
            expect(reduce).to.be.a("function");
            expect(reduce).to.be.a.instanceOf(Function);
        }),

        it("accepts and array of promises and should return a reduced value", async () => {
            const pArr = [];
            for (let i=0; i<5; i++) {
                pArr.push(new Promise((resolve, reject) => {
                    return setTimeout(() => {
                        return resolve(i);
                    }, 0);
                }));
            };
            
            expect(await reduce(pArr, async (acc, curr) => {
                acc = await acc;
                curr = await curr;
                return parseInt(curr as string) + parseInt(acc as string);
            }, 0)).to.deep.equal(10);
        }),

        it("accepts a mixed array of promises and primitives and should return a reduced value", async () => {
            const pArr = [];
            for (let i=0; i<5; i++) {
                pArr.push(new Promise((resolve, reject) => {
                    return setTimeout(() => {
                        return resolve(i);
                    }, 0);
                }));
            };
            pArr.push(5);
            pArr.push(10);
            
            expect(await reduce(pArr, async (acc, curr) => {
                acc = await acc;
                curr = await curr;
                return parseInt(curr as string) + parseInt(acc as string);
            }, 0)).to.deep.equal(25);
        }),

        it("should throw an error",  async function () {
            try{
                await reduce([2, createPromise("reject", 1000), 1], async (acc, curr) => {
                    return parseInt(curr as string) + parseInt(acc as string);
                }, 0);
            }catch(err){
                expect(err).to.be.equal("Promise rejected");
            }
        });
    });
});