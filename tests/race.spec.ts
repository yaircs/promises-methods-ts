import { expect } from "chai";
import { race } from "../src/promises-lib/race";
import { delay } from '../src/promises-lib/utils';

function createPromise(operation: string, ms: number) {
    return new Promise((resolve, reject) => {
        delay(ms)
            .then(() => {
                if (operation === "reject") {
                    throw new Error("Promise rejected");
                }
                resolve(operation);
            })
            .catch((err)=>reject(err.message));
    });
}

describe("tests of 'race' function", () => {
    context("race module", () => {

        it("should be a function", () => {
            expect(race).to.be.a("function");
            expect(race).to.be.a.instanceOf(Function);
        }),

        it("accepts an array of promises should return the value of the first promise to be resolved", async () => {
            const pArr = [];
            for (let i=0; i<5; i++) {
                pArr.push(new Promise((resolve, reject) => {
                    return setTimeout(() => {
                        return resolve(i);
                    }, i*1000);
                }));
            };
            
            expect(await race(pArr)).to.deep.equal(0);
        }),

        it("should throw an error",  async function () {
            try{
                await race([2, createPromise("reject", 1000), 1]);
            }catch(err){
                expect(err).to.be.equal("Promise rejected");
            }
        });
    });
});