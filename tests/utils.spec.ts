import { expect } from "chai";
import { delay, random } from "../src/promises-lib/utils";


describe("tests of 'delay' function", () => {
    context("delay module", () => {

        it("should be a function", () => {
            expect(delay).to.be.a("function");
            expect(delay).to.be.a.instanceOf(Function);
        }),
        
        it("should wait 1.5 seconds", async function() {
            const start = new Date();
            await delay(1500);
            expect((Number(new Date()) - Number(start))).to.be.greaterThanOrEqual(1500);
        });
    });
});

describe("tests of 'random' function", () => {
    context("random module", () => {

        it("should be a function", () => {
            expect(random).to.be.a("function");
            expect(random).to.be.a.instanceOf(Function);
        }),
        
        it("should generate random num between 1 - 5", async function() {
            expect(random(5, 1)).to.be.lessThan(6).to.be.greaterThan(0);
        });
    });
});
