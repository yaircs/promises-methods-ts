import { expect } from "chai";
import { mapSeries } from "../src/promises-lib/mapSeries";
import { delay } from '../src/promises-lib/utils';

function createPromise(operation: string, ms: number) {
    return new Promise((resolve, reject) => {
        delay(ms)
            .then(() => {
                if (operation === "reject") {
                    throw new Error("Promise rejected");
                }
                resolve(operation);
            })
            .catch((err)=>reject(err.message));
    });
}

describe("tests of 'mapSeries' function", () => {
    context("mapSeries module", () => {

        it("should be a function", () => {
            expect(mapSeries).to.be.a("function");
            expect(mapSeries).to.be.a.instanceOf(Function);
        }),

        it("accepts an array of promises and should return a resolved mapped arr", async () => {
            const pArr = [];
            for (let i=0; i<5; i++) {
                pArr.push(new Promise((resolve, reject) => {
                    return setTimeout(() => {
                        return resolve(i);
                    }, 0);
                }));
            };
        
            expect(
                await mapSeries(pArr, async (item) => {
                    return Number(await item) * 2;
                })
            ).to.deep.equal([0,2,4,6,8]);
        }),

        it("accepts a mixed array of promises and primitives and should return a resolved mapped arr", async () => {
            const pArr = [];
            for (let i=0; i<5; i++) {
                pArr.push(new Promise((resolve, reject) => {
                    return setTimeout(() => {
                        return resolve(i);
                    }, 0);
                }));
            };
            pArr.push(10);
            pArr.push(20);
            
            expect(await mapSeries(pArr, async (item) => {
                return Number(await item) * 2;
            })).to.deep.equal([0,2,4,6,8,20,40]);
        }),

        it("should throw an error",  async function () {
            try{
                await mapSeries([2, createPromise("reject", 1000), 1], async (item) => {
                    return Number(await item) * 2;
                });
            }catch(err){
                expect(err).to.be.equal("Promise rejected");
            }
        });

    });
});

