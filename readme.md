# @yairrr/promises-lib
Reimplementation of an Async library to resolve an iterable of promises.

## Get Started
``` $ npm i @yairrr/promises-lib ```

## available functions:
```

all(resolvedArr: Iterable<any> | Promise<Iterable<any>>): Promise<Array<unknown>>

each(promiseIterable: Iterable<unknown> | Promise<Iterable<unknown>>, callback: (item: unknown, index: number, length: number) => unknown):Promise<unknown[]>

props(promisesObj: pObj): Promise<pObj>

mapSeries(promiseIterable: Iterable<unknown> | Promise<Iterable<unknown>>, callback: (item: unknown, index: number) => unknown):Promise<Array<unknown>>

mapParallel(promiseIterable: Iterable<any> | Promise<Iterable<any>>, callback: (item: unknown) => unknown):Promise<Array<unknown>>

filterSeries(promiseIterable: Iterable<any> | Promise<Iterable<any>>, callback: (item: unknown, index: number) => unknown):Promise<Array<unknown>>

filterParallel(promiseIterable: Iterable<any> | Promise<Iterable<any>>, callback: (item: unknown) => unknown):Promise<Array<unknown>>

reduce(promiseIterable: Iterable<any> | Promise<Iterable<any>>, callback: (agg: unknown, item: unknown) => unknown, initial: unknown): Promise<Array<unknown>>

race(promiseIterable: Iterable<any> | Promise<Iterable<any>>): Promise<any>

some(promiseIterable: Iterable<any> | Promise<Iterable<any>>, 
count:number): Promise<any>

delay(ms: number): Promise<NodeJS.Timeout>

echo(msg: string, ms: number): Promise<string>

random(max: number, min: number=0): number
```

## usage 
```
import * as P from '@yairrr/promises-lib'

```
