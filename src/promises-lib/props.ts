export interface pObj {
    [key: string]: Promise<any> | unknown
}

export async function props(promisesObj: pObj): Promise<pObj> {
    promisesObj = await promisesObj;
    const resObj: pObj = {};
    for (const key of Object.keys(promisesObj)) {
        resObj[key] = await promisesObj[key];
    }  
    return resObj;
  }

