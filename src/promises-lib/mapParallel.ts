export async function mapParallel(promiseIterable: Iterable<any> | Promise<Iterable<any>>, callback: (item: unknown) => unknown):Promise<Array<unknown>> {
    promiseIterable = await promiseIterable;
    const resArr = [];
    const pending = Array.from(promiseIterable, item => callback(item));
    for (const promise of pending) {
        resArr.push(await promise); 
    }
    return resArr;
}
