export async function each (promiseIterable: Iterable<unknown> | Promise<Iterable<unknown>>, callback: (item: unknown, index: number, length: number) => unknown):Promise<unknown[]> {
    promiseIterable = await promiseIterable;
    const resArr = [];
    for (const [i, item] of Object.entries(promiseIterable)) {  
        const resolved = await callback(item, Number(i), Object.entries(promiseIterable).length);
        resArr.push(resolved);
    }
    return resArr;
}