export async function filterSeries(promiseIterable: Iterable<any> | Promise<Iterable<any>>, callback: (item: unknown, index: number) => unknown):Promise<Array<unknown>> {
   promiseIterable = await promiseIterable;
    const resolvedArr = [];
    for (const [i, item] of Object.entries(promiseIterable)) { 
        if(await callback(item, Number(i)) === true) resolvedArr.push(await item);
    }
    return resolvedArr;

}

