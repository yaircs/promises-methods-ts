export async function mapSeries(promiseIterable: Iterable<unknown> | Promise<Iterable<unknown>>, callback: (item: unknown, index: number) => unknown):Promise<Array<unknown>> {
    promiseIterable = await promiseIterable;
    const resArr = [];
    for (const [i, item] of Object.entries(promiseIterable)) { 
        const resolved = await callback(item, Number(i));
        resArr.push(resolved);
    }
    return resArr;
}