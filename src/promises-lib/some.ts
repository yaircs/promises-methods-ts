export async function some(promiseIterable: Iterable<any> | Promise<Iterable<any>>, count:number): Promise<any> {
    promiseIterable = await promiseIterable;
    const resArr: unknown[] = [];

    return await new Promise((resolve)=>{
        (promiseIterable as Array<any>).forEach(async promise => {
            resArr.push(await promise);
            if(resArr.length === count) resolve(resArr); 
        });
    });
}