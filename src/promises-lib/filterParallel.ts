export async function filterParallel(promiseIterable: Iterable<any> | Promise<Iterable<any>>, callback: (item: unknown) => unknown):Promise<Array<unknown>> {
    promiseIterable = await promiseIterable;
    const resolvedArr = Array.from(promiseIterable, item => callback(item));

    const resultArr = [];
    for (const [i, item] of Object.entries(promiseIterable)) {
        if (await resolvedArr[Number(i)] === true) {
            resultArr.push(await item);
        }
    }
    return resultArr;
}

