export async function all(resolvedArr: Iterable<any> | Promise<Iterable<any>>): Promise<Array<unknown>> {
    resolvedArr = await resolvedArr;
    const resArr = [];
    for (const [i, promise] of Object.entries(resolvedArr)) {
        const resolved = await promise;
        resArr.push(resolved);
    }
    return resArr;
}
