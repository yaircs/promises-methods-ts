
export async function reduce(promiseIterable: Iterable<any> | Promise<Iterable<any>>, callback: (agg: unknown, item: unknown) => unknown, initial: unknown): Promise<Array<unknown>> {
    promiseIterable = await promiseIterable;
    let aggregator = initial || (promiseIterable as Array<any>)[0];

    for (const item of promiseIterable) {
        aggregator = await callback(aggregator, item);
    }
    
    return aggregator;
}

