export async function race(promiseIterable: Iterable<any> | Promise<Iterable<any>>): Promise<any> {
    promiseIterable = await promiseIterable;
    return await new Promise((resolve)=>{
        (promiseIterable as Array<any>).forEach(async promise => {
            resolve(await promise);
        });
    });
}